var CACHE_NAME = 'straight-media-cache';
var resources = [
  'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css',
  'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js',
  'https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js',
  'https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css',
  '/images/logo.jpg',
  '/manifest.json'
];

// Open + Cache + Verify cache
self.addEventListener('install', function(event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function(cache) {
        // console.log('Opened cache');
        return cache.addAll(resources);
      })
  );
});

// Load cache if present otherwise install
self.addEventListener('fetch', function(event) {
    event.respondWith(
      caches.match(event.request)
        .then(function(response) {
          // Cache hit - return response
          if (response) {
            return response;
          }
          return fetch(event.request);
        }
      )
    );
  });
